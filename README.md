# Vault demo

A demo to show that GitLab CI can authenticate to Vault and consume a secret.

## Setup

This setup can be done manually, follow the steps below.
This setup can also be done using Terraform. See `*.tf`.

> Note: When using Terraform, please setup a Vault (dev) server first. (manually.)

### Prepare a Vault server

This step has to be done for both the manual and automatic (Terraform) setup.

```shell
vault server -dev -dev-listen-address=0.0.0.0:8200 -dev-root-token-id simple
```

Append ` -log-level debug` for more verbose logging.

> Note: This has to run on a GitLab available IP address.

### Configure JWT

From here on, either follow the manual steps or the automatic (Terraform) setup.

```shell
export VAULT_ADDR=http://127.0.0.1:8200
vault auth enable jwt
vault write auth/jwt/config oidc_discovery_url="https://gitlab.com" bound_issuer="https://gitlab.com" default_role="vault-demo"
```

### Create a secret

```shell
vault secrets enable kv-v2
vault kv put -mount=kv-v2 secret password=SuPeRsEcUrE
```

### Create a Vault policy

```shell
vault policy write my-policy - <<EOF
# path "kv-v2/data/secret" {
path "*" {
  capabilities = ["read"]
}
EOF
```

### Create a OIDC role

```shell
vault write auth/jwt/role/vault-demo - <<EOF
{
  "role_type": "jwt",
  "policies": ["my-policy"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims_type": "glob",
  "bound_claims": {
    "project_id": "52949741"  # This refers to a specific GitLab project id. Can be found in the source of the HTML page.
  }
}
EOF
```

## Run the demo

See in the [pipeline](https://gitlab.com/robertdebock-iac/vault-demo/-/pipelines) if a secret can be consumed.
