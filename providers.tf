terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }
}

provider "vault" {
  address = "http://65.109.172.47:8200"
  token   = "simple"
}
