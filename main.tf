resource "vault_jwt_auth_backend" "default" {
    description         = "GitLab JWT Auth"
    path                = "jwt"
    oidc_discovery_url  = "https://gitlab.com"
    bound_issuer        = "https://gitlab.com"
    default_role        = "vault-demo"  # Can be dynamic, but would result in a cycle.
}

resource "vault_mount" "default" {
  path        = "kv-v2"
  type        = "kv"
  options     = { version = "2" }
  description = "KV Version 2 secret engine mount"
}

resource "vault_kv_secret_v2" "default" {
  mount                      = vault_mount.default.path
  name                       = "secret"
  data_json                  = jsonencode(
  {
    password = "SuPeRsEcUrE"
  })
}

resource "vault_policy" "default" {
  name = "my-policy"
  policy = <<EOT
path "kv-v2/data/secret" {
  capabilities = ["read"]
}
EOT
}

resource "vault_jwt_auth_backend_role" "default" {
  backend           = vault_jwt_auth_backend.default.path
  role_name         = vault_jwt_auth_backend.default.default_role
  role_type         = "jwt"
  token_policies    = [vault_policy.default.name]
  user_claim        = "user_email"
  bound_claims_type = "glob"

  bound_claims = {
    project_id = "52949741"
  }
}
